module br.star;

private import util.parts;
private import util.particle;
private import br.shapeImpl;
private import br.screen;
private import br.mainloop;
private import br.gamemanager;
private import br.particleImpl;
private import br.sound;


public class Star:Parts{
	public:
	Vector3 vel;
	int hp;
	public this(Vector3 pos ,Vector3 vel){
//		collisionManager.add(this ,collisionManager.kind.SHIP ,2);
//		collisionManager.add(this ,collisionManager.kind.SHOT ,2);
		collisionManager.add(this ,collisionManager.kind.STAR ,2);
		this.pos = cast(Vector3)pos.clone();
		rpos = cast(Vector3)pos.clone();
		this.vel = cast(Vector3)vel.clone();
		
		shape = StarShape.getShape();
		size = 30.0f;
		drawing =  POLYGON | WIRE;
		collisionRange = 40;
		R = 1.0;G =1.0;B = 0.5;alpha = 1.0;
		wR = 0.5;wG = 0.5;wB = 0.1;walpha = 1.0;
		
//		poseZ = atan2(vel.y,vel.x)*180.0/PI;
		starManager.add(this);
		
		hp = 3;
	}
	public void move(){
		super.move();
		if((rpos.y+size < screen.GAME_DOWN || screen.GAME_UP < rpos.y-size ||
			screen.GAME_RIGHT  < rpos.x-size || rpos.x+size < screen.GAME_LEFT ||
			screen.GAME_NEAR < rpos.z - size || rpos.z + size < screen.GAME_FAR
			
		)){
//			ship.starnum = 0;
//			rankdown();
			vanish();
		}
	}
	
	public override void reportCollision(int kind ,Parts p){
		Sound_PlaySe(se_kind.STAR);
		destroy();
	}
	public void destroy(){
		super.destroy();
		makeParticleByShape(shape,4, size*0.7f ,WIRE |POLYGON ,ship.rpos ,new Vector3() ,R ,G ,B ,wR,wG,wB,alpha);

	}
	
}

public class Star1:Star{
	public this(Vector3 pos ,Vector3 vel){
		super(pos ,vel);
	}
	public void move(){
		super.move();
		
		pos += mulsp(vel);
		
		poseZ += mulsp(3f);
	}
	
}

public class Star2:Star{
	public this(Vector3 pos ,Vector3 vel){
		super(pos ,vel);
	}
	public void move(){
		super.move();
		
		Vector3 t = ship.rpos -rpos;
		float r = t.size();
		pos += mulsp(t/(r*r) * 3200.0f);
		
		poseZ += mulsp(3f);
	}
	
}