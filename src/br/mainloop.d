module br.mainloop;
private import opengl;
private import SDL;
private import br.ship;
private import util.key;
private import util.mouse;
private import std.string;

private import br.screen;
private import br.gamemanager;

private import util.parts;
private import std.math;
private import util.vector;
private import util.matrix;
private import util.log;


public class Mainloop{
	

	static int count;

	private:
	Parts ship;
	//Parts[10] append;
	
	public:
	const int INTERVAL = 16;
	const int MAX_SKIP_FRAME = 5;
	GameManager gm;
	
	bool accframe;
	Key key;
	Mouse mouse;
	bool active;
	uint interval;
	uint maxSkipFrame;
	
	public this(Key key ,Mouse mouse){
		this.key = key;
		this.mouse = mouse;
		
		
	}
	public void loop(){
		
		gm = new GameManager(key ,mouse);
//		prefManager = new PrefManager;
		
//		prefManager.load();
		
		long prvTickCount = 0;
		long nowTick;
		int frame;
		
		//partsManager = new PartsManager(128);
		gm.start();
		
		active = true;
		accframe = false;
		interval = INTERVAL;
		maxSkipFrame = MAX_SKIP_FRAME;
		SDL_Event event;
		SDLKey *sdlkey;
		GLubyte mask[128];
		Uint32 prevTime;
		//count = 0;
		
		bool done = false;
		//for(int iCount = 0 ; iCount < 128 ; iCount++) mask[iCount] = 0xF0;
		while(!done){
			
			//prevTime = SDL_GetTicks();
			
			//SDL_UpdateRect(gScreenSurface,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
			
			while(SDL_PollEvent(&event)){
				//event.type = SDL_USEREVENT;
				key.handleEvent(&event);
				
				switch(event.type){
					case SDL_QUIT:
						done = true;
						break;

					case SDL_ACTIVEEVENT:
						switch(event.active.state){
						
						case SDL_APPACTIVE:
							if(event.active.gain){
								//active = true;
							}else active = false;
							break;
							
						case SDL_APPINPUTFOCUS:
							if(event.active.gain){
								//active = true;
							}else active = false;
							
							break;
						
						case SDL_APPMOUSEFOCUS:
							if(event.active.gain)active = true;
							else active = false;
							break;
							
						default:break;
						}
						break;
				
					//case SDL_APPINPUTFOCUS:
						
					case SDL_MOUSEBUTTONDOWN:
						active = true;
						break;
					case SDL_KEYDOWN:
						
						
						sdlkey=&(event.key.keysym.sym);
						if(*sdlkey==SDLK_ESCAPE){
							done = true;
						}else if(*sdlkey==SDLK_F9)screen.toggleFullScreen();
//						else if(*sdlkey==SDLK_F12)screen.saveBMP("screenshot.bmp");
						else if(*sdlkey==SDLK_F5)SDL_WM_IconifyWindow();    
						else if(*sdlkey==SDLK_RETURN){
							gm.returnPushed();
						}
						break;
						
					/*case SDL_VIDEORESIZE:
						{
							SDL_ResizeEvent re = event.resize;
							screen_width = re.w;
							screen_height = re.h;
						}
						*/
						default:break;
				}
			}
			
			
			nowTick = SDL_GetTicks();
			frame = cast(int) (nowTick-prvTickCount) / interval;
			
//			Log_write(d);
			if(frame <= 0){
				frame = 1;
				SDL_Delay(cast(uint)(prvTickCount+interval-nowTick));
				if (accframe) {
					prvTickCount = SDL_GetTicks();
				} else {
					prvTickCount += interval;
				}
			
			}else if (frame > maxSkipFrame) {
				frame = maxSkipFrame;
				prvTickCount = nowTick;
			} else {
				prvTickCount += frame * interval;
			}
			if(active){
				for (int i = 0; i < frame; i++) {
					gm.move();
				}
			}
			
			//active code
			//glLoadIdentity();
//			screen.clear();
			//partsManager.move();
			//partsManager.draw();
			
			gm.draw();
			screen.flip();
			
			
			
			
			
			
			
			
		}
//		prefManager.save();
		gm.close();
		SDL_Quit();
	}
}
	
	