module br.cursor;

private import opengl;
private import util.parts;
private import util.particle;
private import util.mouse;
private import br.shapeImpl;
private import br.screen;
private import br.mainloop;
private import br.gamemanager;



public class Cursor:Parts{
	Mouse mouse;
	float ddeg;
	public this(Mouse mouse){
		this.mouse = mouse;
		MouseState ms = mouse.getState();
		
		this.pos = new Vector3(ms.x-Screen.SCREEN_WIDTH/2,-(ms.y-Screen.SCREEN_HEIGHT/2),-800.0f);
		rpos = cast(Vector3)pos.clone();
		
		shape = new SH_Star(1);
		size = 0.0f;
		drawing =  POLYGON | WIRE;
		R = 0.3;G =0.8;B = 0.6;alpha = 1.0;
		walpha = 0.0;
		
		ddeg = 0.0f;
		
		cursorManager.add(this);
		
		Parts p = new CursorAppend();cursorManager.add(p);
		addChild(p, "app1", 40 ,ENGAGED);
		p = new CursorAppend();cursorManager.add(p);
		addChild(p, "app2", 40 ,ENGAGED ,matRotateZ(120));
		p = new CursorAppend();cursorManager.add(p);
		addChild(p, "app3", 40 ,ENGAGED,matRotateZ(-120));
		
		
	}
	
	public void move(){
		MouseState ms = mouse.getState();
		
		super.move();
		
		pos = new Vector3(ms.x-Screen.SCREEN_WIDTH/2,-(ms.y-Screen.SCREEN_HEIGHT/2),-800.0f);
		
		if(pos.x > screen.GAME_RIGHT)pos.x = screen.GAME_RIGHT;
		if(pos.x < screen.GAME_LEFT)pos.x = screen.GAME_LEFT;
		if(pos.y < screen.GAME_DOWN)pos.y = screen.GAME_DOWN;
		if(pos.y > screen.GAME_UP)pos.y = screen.GAME_UP;
		
		float d = (rpos - ship.rpos).size();
		
		if(d < 200)ddeg = 30.0f;
		else if(d < 350)ddeg = 30.0f * (350.0f-d) / 150.0f;
		else ddeg = 0.0f;
		
		
		this.dist = 40.0f * (cos(cast(double)((30.0f-ddeg) / 30.0f)*PI/2.0f) * 0.4f + 0.6f);
		this.R = 0.3f + (sin(cast(double)((30.0f-ddeg) / 30.0f)*PI/2.0f) * 0.5f);
		this.G = 0.3f + (cos(cast(double)((30.0f-ddeg) / 30.0f)*PI/2.0f) * 0.5f);
		this.B = 0.3f + (cos(cast(double)((30.0f-ddeg) / 30.0f)*PI/2.0f) * 0.3f);
		
		linkZ += 2.0f;
	}
	
	public override void reportCollision(int kind ,Parts p){
//		destroy();
	}

	
}

public class CursorAppend:Parts{
	private:
	float fixDist;
	public this(){
		drawing =   POLYGON | WIRE;
		size =  20;//20;
		shape = new SH_Regular(3);
		size = 20.0f;
		poseZ = 180.0;		
		R = 0.3;G =0.8;B = 0.6;alpha = 1.0;
		wR = 0.0;wG =0.0;wB = 0.0;walpha = 1.0;
		
	}
	public void move(){
		super.move();
		if(cnt == 1)fixDist = dist;
//		dist = fixDist * (sin(cast(double)(cnt % 120)*PI/60.0f) * 0.2f + 0.8f);
		if(parent !is null){
			this.dist = parent.dist;
			this.R =parent.R;this.G=parent.G;this.B=parent.B;
		}
		

	}
}