module br.pattern;

private import util.parts;
private import util.particle;
private import util.collision;
private import br.shapeImpl;
private import br.screen;
private import br.mainloop;
private import br.gamemanager;



public class Pattern:Parts{
	public:
	Vector3 vel;
	bool border;
//	int hp;
	public this(Vector3 pos){

		this.pos = cast(Vector3)pos.clone();
		rpos = cast(Vector3)pos.clone();
	
		patternManager.add(this);
		border = true;
//		hp = 3;
	}
	
	public void move(){
		super.move();
		if(border && (rpos.y+size < screen.GAME_DOWN || screen.GAME_UP < rpos.y-size ||
			screen.GAME_RIGHT  < rpos.x-size || rpos.x+size < screen.GAME_LEFT ||
			screen.GAME_NEAR < rpos.z - size || rpos.z + size < screen.GAME_FAR
			
		)){
			vanish();
		}
		
//		poseX += 2f;
//		poseY += 1f;
	}
	public override void reportCollision(int kind ,Parts p){
		
	}
	public void destroy(){
		super.destroy();
//		makeParticle(shape, size ,WIRE ,rpos ,new Vector3() ,rpose ,R ,G ,B ,alpha);

	}
	
}

public class Pattern1:Pattern{
	public:
	Vector3 vel;
	
	public this(Vector3 pos ,Vector3 vel){
		super(pos);
		shape = TriangleShape.getShape();
		size = 80.0f;
		drawing =  POLYGON;
		R = 130.0f/255.0f;G =240.0f/255.0f;B = 255.0f/255.0f;alpha = 1.0;
		
		this.vel = cast(Vector3)vel.clone();
		poseZ = atan2(vel.y,vel.x)*180.0/PI;
	}
	public void move(){
		super.move();
		if(cnt  == 60){
			Vector3 v = vec3Normalize(ship.rpos -rpos) * 1.0f;
//			new Bullet(rpos ,v);
		}
		
		pos += mulsp(vel);
//		vel *= 1.02f;
		
		poseZ += mulsp(1.0f);
		
	}
	
}

public class Pattern2:Pattern{
	public:
	Vector3 vel;
	
	public this(Vector3 pos ,Vector3 vel){
		super(pos);
		shape = SquareShape.getShape();
		size = 60.0f;
		drawing =  WIRE;
		R = 130.0f/255.0f;G =240.0f/255.0f;B = 255.0f/255.0f;alpha = 1.0;
		
		this.vel = cast(Vector3)vel.clone();
		poseZ = 45;
	}
	public void move(){
		super.move();
		
		
		pos += mulsp(vel);

		
	}
	
}

public class Pattern3:Pattern{
	public:
//	Vector3 vel;
	static float RADIUS;
	public this(){
		super(new Vector3());
		shape = SquareShape.getShape();
		size = 80.0f;
		drawing =  WIRE;
		R = 130.0f/255.0f;G =240.0f/255.0f;B = 255.0f/255.0f;alpha = 1.0;
		
		poseX = 90;
		poseZ = 90;
		poseY =45;
	}
	public void move(){
		super.move();
		dist = RADIUS;
//		poseZ ++;
//		pos += mulsp(vel);

		
	}
	
}
public class Pattern4:Pattern{
	public:
//	Vector3 vel;
	
	public this(Vector3 pos){
		super(pos);
		shape = SquareShape.getShape();
		size = 0.0f;
		drawing =  WIRE;
		
	}
	public void move(){
		super.move();
		linkZ += mulsp(0.4);
		linkY += mulsp(0.1);
	}
	
}