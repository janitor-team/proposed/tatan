module br.shapeImpl;

private import std.math;
private import util.shape;
public class BaseOfWing{
	private{
		static Shape shape;
		static  float[][] a = 
		[
    	    [-0.2 ,0 ,0.2   ],
					[-0.2 ,0 ,0.2 ]
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ],
				 [0 ,1.0 ,0 ]
	       
	     ];
	}
	
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
}



public class Wing{
	private{
		static Shape shape;
		static  float[][] a = 
		[
    	    [-1.0 ,-0.5 ,1.0  ],
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ]
	       
	     ];
	}
	
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Tail{
	private{
		static Shape shape;
		static  float[][] a = 
		[
    	    [-1.0 ,-0.5 ,1.0  ],
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ]
	       
	     ];
	}
	
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Tail2D{
	private{
		static Shape shape;
		static  float[] a = 
		
    	    [-1.0 ,-0.5 ,1.0 ,-0.5 ];
	  static float[] b =
		
	       [0 ,0.5 ,0  ,-0.5 ];
	}
	
	protected static void setShape(){
		shape = new Shape2D(a ,b);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Head{
	private{
		static Shape shape;
		static  float[] a = 
    	    [-1.0 ,-0.5 ,1.0 ,-0.5 ]
					;
	  static float[] b =
	       [0 ,0.5 ,0 ,-0.5   ]
	       ;
		static float[] z =
					[-0.5 ,0 ,0.5];
		static float[] scale =
					[0.3 ,1.0 ,0.3];
	}
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return  cast(Shape)shape.clone();
		
	}
}
public class MyShip{
		private{
		static Shape shape;
		static  float[] a = 
    	    [-1.5 ,-0.75 ,1.5 ,-0.75 ]
					;
	  static float[] b =
	       [0 ,0.75 ,0 ,-0.75   ]
	       ;
		static float[] z =
					[-0.75 ,0 ,0.75];
		static float[] scale =
					[0.1 ,1.0 ,0.1];
	}
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class MyShipAppend{
		private{
		static Shape shape;
		static  float[] a = 
    	    [-1.0 ,-0.25 ,1.25 ]
					;
	  static float[] b =
	       [-0.75 ,0.75 ,-0.25 ]
	       ;
		static float[] z =
					[ ,0 ,0.75];
		static float[] scale =
					[1.0 ,0.0];
	}
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class TurretShape1{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.0 ,0.5 ,2.0 ]
				;
	 static float[] b =
	      [0.0 	,0.8 	,0.6 ,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class TurretShape2{
	private:
	static Shape shape;
	static  float[] a = 
	[-1.0 ,-0.3 ,0.8 ,1.5,0.8,-0.3]
   	    
				;
	 static float[] b =
	      [0.0 ,-0.7 ,-0.5 ,0.0 ,0.5 ,0.7]
	      ;
	static float[] z = 
	[-0.6 ,0.0 ,0.6];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class TurretShape3{
	private:
	static Shape shape;
	static  float[] a = 
			[-1.0 ,-0.6 ,0.0 ,0.6 ,0.6 ,2.0 ,2.1 ,2.0 ,1.0 ,2.0 ,2.1 ,2.0 ,0.6 ,0.6 ,0.0 ,-0.6];
  	static float[] b =
		
			[0.0 ,0.6  ,1.0 ,0.6 ,0.6  ,0.5 ,0.3 ,0.1 ,0.0 ,-0.1,-0.3,-0.5,-0.6,-0.6,-1.0,-0.6];
	static float[] z=
	[-0.6 ,-0.4 ,0.0 ,0.4 ,0.6];
	static float[] scale =
	[0.2 ,0.6 ,1.0 ,0.6 ,0.2];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class NoseShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.0 ,-0.3 ,0.8 ,1.5,0.8,-0.3]
   	    
				;
	 static float[] b =
	      [0.0 ,-0.7 ,-0.5 ,0.0 ,0.5 ,0.7]
	      ;
	static float[] z = 
	[-0.6 ,0.0 ,0.6];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class ShipShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.0 ,0.0 ,1.0 ,1.5 ,0.8 ,0.0 ,-0.8]
				;
	 static float[] b =
	      [0.1 	,0.6 ,0.7,0.6 ,0.1 ,-0.7 ,-0.9 ,-0.7]
	      ;
	static float[] z = 
	[-0.2 ,0.0 ,0.2];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Octahedron{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.0 ,0.0 ,1.0]
				;
	 static float[] b =
	      [0.0 	,1.0 	,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class PrismShape{
	private:
	static Shape[] shape;
	static  float[] a;
	 static float[] b;
	static float[] z;
	static float[] scale = 
	[1.0f ,1.0f];
	protected static Shape setShape(int n, float l ,float r){
		a.length = n;
		b.length = n;
		int i = 0;
		for(double rad=PI;rad>-PI+(1e-6);rad -= PI*2.0/cast(double)n){
			a[i] = cos(rad);
			b[i] = sin(rad);
			i++;
		}
		z.length = 2;
		z[0] = l;
		z[1] = r;
		return new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(int n, float l ,float r){
//		if(shape.length<=n)shape.length = n+1;
//		if(shape[n] is null)setShape(n ,l ,f);
//		setShape(n ,l ,f);
		return setShape(n ,l ,r);
		
	}
}


public class Cube{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-0.71f ,-0.71f ,0.71f ,0.71f]
				;
	 static float[] b =
	      [-0.71f ,0.71f ,0.71f ,-0.71f]
	      ;
	static float[] z=
	[-0.71f ,0.71f];
	static float[] scale = 
	[1.0f ,1.0f];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class BulletShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-2.0 ,0.0 ,0.6 ,1.0]
				;
	 static float[] b =
	      [0.0 	,1.0 ,0.6	,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class TubeShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.0 ,1.0 ,1.5]
				;
	 static float[] b =
	      [0.3 	,0.6 ,0.6 ,0.3 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class ArmShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.2 ,0.7 ,1.5]
				;
	 static float[] b =
	      [0.3 	,0.6 ,0.5 ,0.2 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class ArmShape2{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.55 ,-1.5 ,-0.8 ,0.6 ,1.5 ,1.55]
				;
	 static float[] b =
	      [0.0 ,0.4 	,0.6 ,0.55 ,0.45 ,0.0]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class JointShape{
	
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.4 ,-1.2 ,-0.8 ,0.8 ,1.2 ,1.4 ]
				;
	 static float[] b =
	    [0.0 ,0.6 ,1.0 ,0.8 , 0.6 ,0.0]
	      ;
		  /*
	static float[] z = 
	[-0.4 ,0.0 ,0.4 ,0.7 ];
	static float[] scale = 
	[0.7 ,1.0 ,0.8 ,0.3];
	*/
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
	
}
public class NailShape2{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.6 ,-1.4 , 0.5 ,3.0 ]
				;
	 static float[] b =
	    [0.0 ,0.6 ,0.35 ,0.0]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class NailShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.6 ,-1.2 ,3.0 ]
				;
	 static float[] b =
	      [0.3 	,0.6 ,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class BodyShape{
	
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.3,-0.7 ,0.5 ,0.9 ,1.0]
				;
	 static float[] b =
	    [0.0  ,0.6 ,1.0  ,0.7 ,0.5,0.0]
	      ;
		  /*
	static float[] z = 
	[-0.2 ,0.0 ,0.2];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	*/
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
}

public class Head2{
	
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.3 ,-1.1 ,-0.6 ,0.5 ,0.8 ,1.1 ,0.9 ,0.0 ,-1.0]
				;
	 static float[] b =
	    [0.0  ,0.6  ,1.0  ,0.6 ,0.4 ,-0.3 ,-0.4 ,-0.2 ,-0.2]
	      ;
		  
	static float[] z = 
	[-0.6 ,0.0 ,0.6 ];
	static float[] scale = 
	[0.5 ,1.0 ,0.5];
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
	
}

public class StarShape{
	private{
		static Shape shape;
		static  float[] a;
	  static float[] b;
	}
	
	protected static void setShape(){
		shape = new SH_Star(5);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class TriangleShape{
	private{
		static Shape shape;
		static  float[] a;
	  static float[] b;
	}
	
	protected static void setShape(){
		shape = new SH_Regular(3);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class SquareShape{
	private{
		static Shape shape;
		static  float[] a;
	  static float[] b;
	}
	
	protected static void setShape(){
		shape = new SH_Regular(4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class MissileShape{
	private{
		static Shape shape;
		static  float[][] a = 
			[
				[-1.0 ,-0.7 ,-0.4 ,1.5 ,2.0]
	  	   ];
	  	static float[][] b =
			[
				[0.6  ,0.4  ,0.6  ,0.6 ,0.0]
	  	];
	}
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
}

public class OctaShape{
	private:
	static  float[] a;
	 static float[] b=
	 [0.0 	,1.0 	,0.0 ];
	public static Shape getShape(float l ,float r){
		a.length = 3;
		a[0] = -l;a[1] = 0.0f;a[2] = r;
		return new SH_Pole(a ,b ,4);
	}
	
}