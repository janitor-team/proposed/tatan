module br.laserImpl;

private import std.math;
private import util.laser;
private import util.shape;
private import util.vector;
private import util.matrix;
private import util.collision;
private import br.shapeImpl;
private import br.gamemanager;


public Shape LASERSHAPE;
public class LaserImpl:Laser{
	private static const float[][] a=[
	[0.0f ,0.02f  ,1.0f]
	];
	private static const float[][] b=[
	[0.1f ,0.8f ,1.0f]
	];
	public this(float size=20.0f,Vector3 pos = new Vector3() ,Matrix poseBase = new Matrix()){
		super(pos ,poseBase);
		rpos = cast(Vector3)pos.clone();
		drawing = POLYGON;
		if(LASERSHAPE is null){
			LASERSHAPE = (new SH_Pole(a,b,6));//.rotated(PI/2.0,0,1,0);
		}
		shape = cast(Shape)LASERSHAPE.clone();
		shape.scalef(1000.0f ,size ,size);
		
		R = 1.0f;G =0.85f; B= 0.85f;
		
		collisionRange = size/2.0f;
		this.size = 1.0;
		
		laserManager.add(this);
		collisionManager.add(this ,CollisionManager.kind.LASER ,1);
	}
	
	public void move(){
		super.move();
		if(parent is null)vanish();
		poseX ++;
//		pos.y --;
	}
	
}