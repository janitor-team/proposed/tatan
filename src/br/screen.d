module br.screen;
private import opengl;
private import openglu;
private import SDL;
private import hell2;
private import std.string;
private import std.c.stdlib;
private import br.mainloop;


public class Screen{
	public:
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
	const int SCREEN_BPP = 0;

	const int GAME_LEFT = -200;
	const int GAME_RIGHT = 200;
	const int GAME_UP = 240;
	const int GAME_DOWN = -240;
	const int GAME_NEAR = -2;
	const int GAME_FAR = -1600;
	
	const GLfloat ambientCol[] = [1, 1, 1, 0.5]; 
	
	const GLfloat lightPos1[] = [ 0 , 0 , 100 , 0.0 ];
	const GLfloat lightCol1[] = [ 1 , 1 , 1 , 1 ];
	
	const GLfloat lightPos2[] = [ 0 , 0 , 100 , 1.0 ];
	const GLfloat lightCol2[] = [ 0 , 0 , 1 , 1 ];


	static int g_videoFlags = SDL_SWSURFACE|SDL_OPENGL;
	SDL_Surface *gScreenSurface;
	int width;
	int height;
	int bpp;
	
	public this(){
//		setenv("SDL_VIDEODRIVER","directx" ,1);
//		setenv("SDL_AUDIODRIVER","directx" ,1);
		
		if(SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0 ) {
			throw new Exception(
        "Unable to init SDL video: " ~ std.string.toString(SDL_GetError()));
		}
		//info = SDL_GetVideoInfo( );
		
		width = 	SCREEN_WIDTH;
		height = SCREEN_HEIGHT;
		bpp = SCREEN_BPP;
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	//	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
		gScreenSurface = SDL_SetVideoMode(
		width,
		height,
		bpp,
		g_videoFlags
		);
		SDL_WM_SetCaption("tatan", null);
		
		SDL_ShowCursor(SDL_DISABLE);
//		SDL_WM_GrabInput(SDL_GRAB_ON);
		
		
		//glFrustum(0 ,width ,height ,0 ,1 ,400);
		
		//(width - height) / 2 ,0 ,height ,height);
		
		
		glLightfv(GL_LIGHT1 , GL_POSITION , cast(float*)lightPos1);
	glLightfv(GL_LIGHT1 , GL_DIFFUSE , cast(float*)lightCol1);
	glLightfv(GL_LIGHT2 , GL_POSITION , cast(float*)lightPos2);
	glLightfv(GL_LIGHT2 , GL_DIFFUSE , cast(float*)lightCol2);
	glLightfv(GL_LIGHT0 ,GL_AMBIENT ,cast(float*)ambientCol);
	glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
//	glEnable(GL_LIGHT2);
		
		
		glClearColor(0 ,0 ,0,1.0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_NORMALIZE);
//		glEnable(GL_BLEND);
//		glEnable(GL_LINE_SMOOTH);
//		glEnable(GL_POLYGON_SMOOTH);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glFrustum(0 , 4 , 2 , 0 , 2 , 10);
		
//		glClearColor(0.1 ,0.08 ,0 ,1.0);
		
	}
	public void setProjection(){
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		
		glFrustum(GAME_LEFT / 400.0f , GAME_RIGHT / 400.0f , GAME_DOWN / 400.0f , GAME_UP / 400.0f , 2 , 1600);
//		glOrtho(GAME_LEFT, GAME_RIGHT, GAME_DOWN, GAME_UP, 2, 1600);
		glViewport(width/2 + GAME_LEFT ,height/2 + GAME_DOWN ,GAME_RIGHT -GAME_LEFT ,height/2 + GAME_UP);

	}
	public void setModelView(){
		
		glViewport(width/2 + GAME_LEFT ,height/2 + GAME_DOWN ,GAME_RIGHT -GAME_LEFT ,height/2 + GAME_UP);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		glFrustum(GAME_LEFT / 400.0f , GAME_RIGHT / 400.0f , GAME_DOWN / 400.0f , GAME_UP / 400.0f , 2 , 1600);
//		glOrtho(GAME_LEFT, GAME_RIGHT, GAME_DOWN, GAME_UP, 2, 1600);
		glMatrixMode(GL_MODELVIEW);
//		glEnable(GL_DEPTH_TEST);
	}
	/*
	public void setModelView(){
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		
		glOrtho(GAME_LEFT, GAME_RIGHT, GAME_DOWN, GAME_UP, 2, 1600);
		glViewport(width/2 + GAME_LEFT ,height/2 + GAME_DOWN ,GAME_RIGHT -GAME_LEFT ,height/2 + GAME_UP);
		glEnable(GL_DEPTH_TEST);
	}
	*/
	public void setScoreView(){
		
		glMatrixMode(GL_PROJECTION);
		
		glLoadIdentity();
		glOrtho(0, width-GAME_RIGHT, GAME_DOWN, GAME_UP, 2, 1600);
		glViewport(width/2 + GAME_RIGHT ,height/2 + GAME_DOWN ,width-GAME_RIGHT ,height/2 + GAME_UP);
		glEnable(GL_DEPTH_TEST);
	}
	public void setClearColor(double R ,double G,double B,double alpha){
		glClearColor(R ,G ,B ,alpha);
	}
	public void clear() {
    glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
  }
	public void flip() {
    //handleError();
    SDL_GL_SwapBuffers();
  }
	public void handleError() {
    GLenum error = glGetError();
    if (error == GL_NO_ERROR)
      return;
    closeSDL();
    throw new Exception("OpenGL error(" ~ std.string.toString(error) ~ ")");
  }
	public void closeSDL() {
    close();
    SDL_ShowCursor(SDL_ENABLE);
  }
	public  void close() {
    
  }
	public void toggleFullScreen()
	{
		if(g_videoFlags && SDL_FULLSCREEN) SDL_ShowCursor(SDL_DISABLE);
		else SDL_ShowCursor(SDL_ENABLE);
		g_videoFlags ^= SDL_FULLSCREEN;
		gScreenSurface = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, g_videoFlags);
		SDL_WM_SetCaption("tatan", null);
		
		SDL_ShowCursor(SDL_DISABLE);
		glLightfv(GL_LIGHT1 , GL_POSITION , cast(float*)lightPos1);
	glLightfv(GL_LIGHT1 , GL_DIFFUSE , cast(float*)lightCol1);
	glLightfv(GL_LIGHT2 , GL_POSITION , cast(float*)lightPos2);
	glLightfv(GL_LIGHT2 , GL_DIFFUSE , cast(float*)lightCol2);
	glLightfv(GL_LIGHT0 ,GL_AMBIENT ,cast(float*)ambientCol);
		glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
//	glEnable(GL_LIGHT2);
		
		
		glClearColor(0 ,0 ,0,1.0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_NORMALIZE);
//		glEnable(GL_BLEND);
//		glEnable(GL_LINE_SMOOTH);
//		glEnable(GL_POLYGON_SMOOTH);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		Hell_loadTexture("back", "image/back.bmp");
		Hell_loadFont();
	}
	public void saveBMP(char[] name){
		SDL_SaveBMP(gScreenSurface, "screenshot.bmp");
	}
}