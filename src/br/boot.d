module br.boot;
private import br.mainloop;
private import util.key;
private import util.mouse;
private import SDL;
private Mainloop mainloop;
private Key key;
private Mouse mouse;

private:
import std.c.windows.windows;

extern (C) void gc_init();
extern (C) void gc_term();
extern (C) void _minit();
extern (C) void _moduleCtor();

extern (Windows)
public int WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	int result;
  gc_init();
	_minit();
	try
	{
	_moduleCtor();
	result = user_start();
	SDL_Quit();
	}catch (Object o)
	{
	MessageBoxA(null, cast(char *)o.toString(), "Error",
			MB_OK | MB_ICONEXCLAMATION);
	result = 0; 	// failed
	}
	gc_term();
	return result;
}

int user_start(){
	double d;

	version (X86) {
    short cw;
    asm { fnstcw cw; }
    cw &= ~1;
    asm { fldcw cw; }
  }

	key = new Key();
	mouse = new Mouse();
	mainloop = new Mainloop(key ,mouse);
	mainloop.loop();
	return 0;
}
