module util.particle;
private import opengl;
private import std.math;
private import util.actor;
private import util.parts;
private import util.vector;
private import util.shape;
private import util.matrix;
private import util.log;
private import br.gamemanager;
/*
public class Particle_old:Parts{
	public Vector3 velocity;
	public double dposeX ,dposeY ,dposeZ;
	public this(Shape shape, double size ,ubyte drawing ,Vector3 pos ,Vector3 vel ,Matrix rposeBase ,double R ,double G ,double B ,double alpha){
		this.shape = cast(Shape)shape.clone();
		this.size = size;
		this.pos = cast(Vector3)pos.clone();
		this.velocity = cast(Vector3)vel.clone();
		setPoseBase(rposeBase);
		this.R = R;
		this.G = G;
		this.B = B;
		this.alpha = alpha;
		this.drawing = drawing;
		dposeX = rand.nextFloat(10);
		dposeY = rand.nextFloat(10);
		dposeZ = rand.nextFloat(10);
	}
	public void move(){
		super.move();
		if(cnt > 3){
			pos += velocity;
			poseX += dposeX;
			poseY += dposeY;
			poseZ += dposeZ;
			
			velocity *= 0.95;
		}
		alpha *= 0.95;
		
		if(alpha < 0.1)vanish();
		
	}

}
*/
public class Particle:Actor{
	public:
	Vector3 velocity;
	double dpose;
	double pose;
//	double dposeX ,dposeY ,dposeZ;
	double poseX ,poseY ,poseZ;
	Matrix poseBase;
	
	Shape shape;
	double size;
	ubyte drawing;
	Vector3 pos;
//	Vector3 vel;
//	Matrix rposeBase;
	double R ,G ,B ,alpha;
	double wR,wG,wB;
	double falpha;
	int cnt;
	
	double firstv;
	private bool _exists;
	
	public this(){
		_exists = false;
	}
	public void set(Shape shape, double size ,ubyte drawing ,Vector3 pos ,Vector3 vel ,Matrix rposeBase ,double R ,double G ,double B ,double wR,double wG,double wB ,double alpha){
		this.shape = cast(Shape)shape.clone();
		this.size = size;
		this.pos = cast(Vector3)pos.clone();
		this.velocity = cast(Vector3)vel.clone();
		this.poseBase = cast(Matrix)rposeBase.clone();
		this.R = R;
		this.G = G;
		this.B = B;
		this.wR=wR;
		this.wG=wG;
		this.wB=wB;
		this.falpha=alpha;
		this.alpha = alpha;
		this.drawing = drawing;
		poseX = 0.0; poseY = 0.0; poseZ = 0.0;
		
		pose = 0.0;
		dpose = rand.nextFloat(20);
		poseX = rand.nextFloat(0.5)+0.5;
		poseY = rand.nextFloat(0.5)+0.5;
		poseZ = rand.nextFloat(0.5)+0.5;
		
		firstv = abs(velocity.x) + abs(velocity.y) + abs(velocity.z);
		
		double dist = sqrt(poseX * poseX + poseY * poseY + poseZ * poseZ);
		if((1e-6) < dist){
			poseX = poseX / dist;
			poseY = poseY / dist;
			poseZ = poseZ / dist;
		}
		/*
		dposeX = rand.nextFloat(10);
		dposeY = rand.nextFloat(10);
		dposeZ = rand.nextFloat(10);
		*/
		cnt = 0;
		_exists = true;
	}
	public void move(){
//		super.move();
		cnt ++;
		if(cnt > 3){
			pos += velocity;
			
			pose += dpose;
			/*
			poseX += dposeX;
			poseY += dposeY;
			poseZ += dposeZ;
			*/
			velocity *= 0.95;
		}
		alpha =  falpha * (abs(velocity.x) + abs(velocity.y) + abs(velocity.z))/firstv;
		
		if(alpha < 0.1)vanish();
		
	}
	public void draw(){
		
//		glDisable(GL_BLEND);
		//glLoadIdentity();
//		double size = 10.0;
//		Matrix rpose = poseBase;
		
		//		Matrix drawpose = rpose
		//Matrix p = matRotateX(rposeX) * matRotateY(rposeY) * matRotateZ(rposeZ) * matScale(size * scaleX ,size * scaleY ,size * scaleZ) * poseBase;
		
		glEnable(GL_BLEND);
		glTranslatef(pos.x ,pos.y ,pos.z);
		glScaled(size ,size ,size);
		glRotated(pose ,poseX ,poseY ,poseZ);
		
		if(shape !is null && (1e-6) < size){
		
//			shape.transformMatrix(rpose);
			
			
			
			
			Vector3[] av = shape.v;
			

			
//			glEnable(GL_POLYGON_SMOOTH);
			
			Vector3 ave;
			if(drawing & Parts.POLYGON){
				polygon[] pr = shape.polygons;
				for(int i = 0; i < pr.length ;i ++){
					Vector3 normal = shape.polygons[i].normal;
					glBegin(GL_POLYGON);
					
						ave = new Vector3((av[pr[i].v[0]] + av[pr[i].v[1]] + av[pr[i].v[2]]) / 3.0);
						double a = fmax(0.0,(1.0 + (pos.z + ave.z) / 1600.0) * alpha);
						glColor4d(R  ,G  ,B ,a);
					//glColor4d(R ,G ,B ,fmax(0.0,(1.0 + (pos.z + ave.z) / 1600.0) * alpha)); //glColor4f(1.0f ,1.0f ,1.0f,(1.0 + (pos.z + ave.z) / 1600.0) * 0.6);
					for(int j = 0;j < 3 ;j ++){
						glVertex3f(av[pr[i].v[j]].x,av[pr[i].v[j]].y,av[pr[i].v[j]].z);
					}
					
					glEnd();
				}
			}
			
			
			glEnable(GL_BLEND);
			glEnable(GL_LINE_SMOOTH);
			if(drawing & Parts.WIRE){
				
				
				int[][] wires = shape.wires;
				
//				double a = fmin(1.0 ,fmax(0.0 ,alpha));
				for(int i = 0; i < wires.length ;i ++){
//					glColor3d(R * a ,G * a ,B * a);
//					glColor4d(R ,G ,B ,fmax(0.0,alpha));
					
					
					

					glBegin(GL_LINE_STRIP);
					
					glVertex3f(0 ,0,0);
					glVertex3f(0 ,0,0);
					
					
					glColor4d(wR ,wG ,wB ,fmax(0.0,alpha));
					
		   			for(int j = 0; j < wires[i].length;j ++){
//						double a = alpha * fmax(0.0,(1.0 + (pos.z + av[wires[i][j]].z + 600.0) / 1200.0));
//						glColor4d(R ,G ,B ,fmax(0.0,a));

	 	  	 			glVertex3f(av[wires[i][j]].x ,av[wires[i][j]].y ,av[wires[i][j]].z);


						//glVertex3f(av[wires[i][j + 1]].x ,av[wires[i][j + 1]].y ,av[wires[i][j + 1]].z);
	  	  			}
					
					
					
					glEnd();
					
					
				
				}
				
			}
			
			
			
			glDisable(GL_LINE_SMOOTH);
			glDisable(GL_BLEND);
			
//			shape.resetVertex();
			
			
		}
		
		
//		drawn = true;
	}
	public void vanish(){
		_exists = false;
	}
	public bool exists(){
		return _exists;
	}

}





public class ParticleManager{
	
	public:
	Particle[] particles;
	//List!(Parts) parts;
	protected:
	//List!(int) van = new List!(int)();
  int particleIdx = 0;
	const int maxParticle;
	public this() {
		maxParticle = 16;
		particles.length = maxParticle;
		foreach(inout Particle p;particles){
			p = new Particle();
		}
		//parts = new List!(Parts)();
		particleIdx = 0;
	}

  public this(int n) {
		maxParticle = n;
		particles.length = maxParticle;
		foreach(inout Particle p;particles){
			p = new Particle();
		}
		//parts = new List!(Parts)();
		particleIdx = 0;
  }

	public Particle getInstance(){
		
		for(int i = 0;i < particles.length;i ++){
			particleIdx ++;
			if(particles.length <= particleIdx)particleIdx = 0;
			if(!particles[particleIdx].exists()){
				return particles[particleIdx];
			}
			
		}
		
		return null;
		
		//if(maxParts <= parts.size)return false;
		//parts.push_back(p);
		
	}
	public void run() {
		
		for (int i = 0;i < particles.length;i ++){
			Particle an = particles[i];
			
    	  	if (an.exists){
				
        		an.move();
    	  	}
			
    	}
		  

   }
		
		
  public void draw() {
		
		for (int i = 0;i < particles.length;i ++){
			Particle an = particles[i];
			if (an.exists){
				glPushMatrix();
       			an.draw();
				glPopMatrix();
			}
		}
  }


  public void clear() {
		foreach(inout Particle a;particles){
			if(a !is null && a.exists){
				a.vanish();
			}
		}
   	//parts.length = 0;
    particleIdx = 0;
  }
  
}