private import opengl;
public abstract class Actor{
	private bool _exists;
	public this(){
		_exists = true;
	}
	public void move();
	public void draw();
	public bool exists(){
		return _exists;
	}
	public void vanish(){
		_exists = false;
	}
}

public class ActorManager{
	public:
	Actor[] actor;
	protected:
  	int actorIdx = 0;
	const int maxActor;
	public this() {
		maxActor = 16;
		actor.length = maxActor;
		actorIdx = 0;
	}

  public this(int n) {
		maxActor = n;
		actor.length = maxActor;
		actorIdx = 0;
  }

	public bool add(Actor a){
		for(int i = 0;i < actor.length;i ++){
			if(actor[actorIdx] is null || !actor[actorIdx].exists()){
				actor[actorIdx] = a;
				return true;
			}
			actorIdx ++;
			if(actor.length <= actorIdx)actorIdx = 0;
		}
		return false;
	}
	public void addForce(Actor a){
		actor[actorIdx] = a;
		actorIdx ++;
	}
	public void move() {
		for (int i = 0;i < actor.length;i ++){
			Actor a = actor[i];
    		if (a !is null && a.exists){
				
        		a.move();
				if(a.exists == false)a = null;
      		}else{
	    }
    }
  }


  public void draw() {
		
    for (int i = 0;i < actor.length;i ++){
		Actor a = actor[i];
      	if (a !is null && a.exists){
			glPushMatrix();
   	  		a.draw();
			glPopMatrix();
      }
    }
		
  }
/*
  public void allDestroy() {
	foreach(inout Actor a;actor){
		if(a !is null && a.exists){
			a.destroy();
		}
	}
   	//parts.length = 0;
    actorIdx = 0;
  }
*/
  public void clear(){
  	foreach(inout Actor a;actor){
		if(a !is null && a.exists){
			a.vanish();
		}
	}
   	//parts.length = 0;
    actorIdx = 0;
  }
  
}