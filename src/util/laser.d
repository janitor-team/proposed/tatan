module util.laser;
private import util.vector;
private import util.basis;
private import util.parts;

public class Laser:Parts{
	public Vector3 laseraim;
	public this(Vector3 pos = new Vector3() ,Matrix poseBase = new Matrix()){
		super(pos ,poseBase);
	}
	public void move(){
		super.move();
		laseraim = poseVector();
	}
	
	
}