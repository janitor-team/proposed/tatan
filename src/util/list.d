module util.list;
private import util.cloneable;
template DeepList(T:Cloneable){
	class DeepList:List{
		void push_back(T t)
		{
		
			if (capacity() == size()) {
			
			int s = size()*2;
			if (s == 0) {
				s++;
			}
			T[] a = new T[s];
			for(int i=0;i<size_;++i)
				a[i] = a_[i];
				a_ = a;
			}
			a_[size_] = cast(T)t.clone();
			size_++;
	}
		void erase(int n)
		{
			for(int i=n ; i<size()-2 ; ++i){
				a_[i] = cast(T)a_[i+1].clone();
			}
			a_[--size_] = T.init; // null;
		}
	}
}
template List(T) { class List {


	T at(int n)
	{ return a_[n]; }

	int size()
	{ return size_; }

	int capacity()
	{
		if (!a_) return 0;
		return a_.length;
	}

	void erase(int n)
	{
		for(int i=n ; i<size()-2 ; ++i){
			a_[i] = a_[i+1];
		}
		a_[--size_] = T.init; // null;
	}


	void push_back(T t)
	{
	
		if (capacity() == size()) {
		
			int s = size()*2;
			if (s == 0) {
				s++;
			}
			T[] a = new T[s];
			for(int i=0;i<size_;++i)
				a[i] = a_[i];
			a_ = a;
		}
		a_[size_] = t;
		size_++;
	}

	void clear() 
	{
		size_ = 0;
		a_ = new T[16];
	}

	bool empty() 
	{	return !(a_); }

	void resize(int n)

	{
		clear();
		if (n==0) return ;
		size_ = n;

		int i=n-1,j=1;
		while (i!=0) { i>>=1; j<<=1; }
		a_ = new T[j];
	}

	this() { clear(); }

private:
	T[] a_;
	int size_;
}}