tatan ver1.0
HIZ

<操作>
 SPACE key　左右移動 move
 MOUSE　　　ショット shoot
 ENTER key  リスタート restart
 ESC key    ゲーム終了 quit
 F9 key     ウインドウ/フルスクリーン切り替え full screen/window

<ルール>
敵とか弾に当たらないように敵を撃って倒しまくろう。
最後のボスを倒せばゲームクリアー。

※☆を10個取るごとにゲームの速度上がって難易度アップ

<起動方法>
tatan.exeをダブルクリック

<アンインストール方法>
tatanフォルダを削除してください。

<感想・苦情>
hiz_oka@yahoo.co.jp
まで。　

<ライセンス>
　やわらかライセンスで公開します。（やわらかライセンス.txt参照）
　ただし、使用素材については配布元の規定に従うものとします。

<謝辞>
　効果音にザ・マッチメイカァズさまの素材を使用させていただきました。
　http://osabisi.sakura.ne.jp/m2/
　音楽に3104式さまの素材を使用させていただきました。
　http://www.geocities.jp/cyber_rainforce/
　D言語ライブラリdhellを使用させていただきました。
　（無理矢理ソースにあわせるために弄ってあります！
　　自分で使用する場合は下記のURLからダウンロードしてください！）
　http://www5.atwiki.jp/yaruhara/pages/80.html

<免責>
 本ソフトによって発生したいかなる問題に対しては
 いっさいの責任を負いかねます。

<更新履歴>
2007/07/29
 released.